const config = require('s-config');
const mysql  = require('promise-mysql');

// "DB_HOST": "localhost",
// "DB_PORT": 3306,
// "DB_USER": "root",
// "DB_PASSWORD": "",
// "DB_DATABASE": "",
// "DB_CONN_LIMIT": 15,

const db = {
	host: config.DB_HOST || 'localhost',
	port: config.DB_PORT || 3306,
	user: config.DB_USER || "root",
	password: config.DB_PASSWORD || "",
	database: config.DB_DATABASE,
	connectionLimit: config.DB_CONN_LIMIT || 15,
};

const dicc = {
	host: 'DB_HOST',
	port: 'DB_PORT',
	user: 'DB_USER',
	password: 'DB_PASSWORD',
	database: 'DB_DATABASE',
	connectionLimit: 'DB_CONN_LIMIT',
};

for(let k in db) {
	if (typeof db[k] === 'undefined') {
		console.error(`El campo "${dicc[k]}" no se encuentra en el archivo de configuración.`);
		process.exit(1);
	}
}

module.exports = mysql.createPool(db);